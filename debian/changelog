moarvm (2022.07+dfsg-1~exp1) experimental; urgency=medium

  * New upstream version 2022.07+dfsg

 -- Mo Zhou <lumin@debian.org>  Fri, 05 Aug 2022 00:00:50 -0700

moarvm (2022.06+dfsg-1~exp1) experimental; urgency=medium

  [ Dominique Dumont ]
  * move README.source in README.source.org

  [ Mo Zhou ]
  * New upstream version 2022.06+dfsg

 -- Mo Zhou <lumin@debian.org>  Thu, 07 Jul 2022 20:45:36 -0700

moarvm (2022.04+dfsg-2) unstable; urgency=medium

  * Upload to unstable.

 -- Mo Zhou <lumin@debian.org>  Sat, 28 May 2022 12:32:54 -0400

moarvm (2022.04+dfsg-1) experimental; urgency=medium

  * New upstream version 2022.04+dfsg

 -- Mo Zhou <lumin@debian.org>  Thu, 19 May 2022 21:18:36 -0400

moarvm (2022.03+dfsg-1) experimental; urgency=medium

  * New upstream version 2022.03+dfsg
  * Add lintian overrides.
  * Upload to experimental.

 -- Mo Zhou <lumin@debian.org>  Fri, 01 Apr 2022 00:13:26 -0400

moarvm (2022.02+dfsg-1) unstable; urgency=medium

  * Upload to unstable.

 -- Mo Zhou <lumin@debian.org>  Tue, 22 Feb 2022 21:44:01 -0500

moarvm (2022.02+dfsg-1~exp1) experimental; urgency=medium

  * New upstream version 2022.02+dfsg
  * Also exclude 3rdparty/mimalloc from upstream source.
  * Disable building with mimalloc.
  * Refresh existing patches with quilt.

 -- Mo Zhou <lumin@debian.org>  Mon, 21 Feb 2022 18:44:49 -0500

moarvm (2021.12+dfsg-1) unstable; urgency=medium

  * Upload to unstable.

 -- Mo Zhou <lumin@debian.org>  Sun, 30 Jan 2022 12:22:56 -0500

moarvm (2021.12+dfsg-1~exp1) experimental; urgency=medium

  * New upstream version 2021.12+dfsg
  * Update upstream signing key.
  * Refresh existing patches with quilt.
  * Refresh d/copyright with cme update dpkg-copyright.

 -- Mo Zhou <lumin@debian.org>  Fri, 21 Jan 2022 11:17:16 -0500

moarvm (2021.09+dfsg-1) unstable; urgency=medium

  * New upstream version 2021.09+dfsg
  * copyright:
    * exclude junit directory
    * updated with cme
  * copyright-scan-patterns: ignore more binary files
  * fix.scanned.copyright: resolve and/or issue
  * control:
    * bump debhelper to 13
    * Remove Robert from Uploaders (Closes: 994649)
    * declare compliance with policy 4.6.0
    * drop obsolete versioned dependencies

 -- Dominique Dumont <dod@debian.org>  Sun, 26 Sep 2021 16:22:12 +0200

moarvm (2020.12+dfsg-1) unstable; urgency=medium

  * New upstream version 2020.12+dfsg
  * control: set Rules-Requires-Root to no

 -- Dominique Dumont <dod@debian.org>  Thu, 07 Jan 2021 12:54:05 +0100

moarvm (2020.11+dfsg-1) unstable; urgency=medium

  * New upstream version 2020.11+dfsg
  * declare compliance with policy 4.5.1

 -- Dominique Dumont <dod@debian.org>  Wed, 02 Dec 2020 11:49:25 +0100

moarvm (2020.10+dfsg-1) unstable; urgency=medium

  * New upstream version 2020.10+dfsg

 -- Dominique Dumont <dod@debian.org>  Sun, 01 Nov 2020 12:54:15 +0100

moarvm (2020.09+dfsg-1) unstable; urgency=medium

  * New upstream version 2020.09+dfsg
  * use relative path target in install file
  * skip copyright scan on a file that contain misleading info.
  * refreshed copyright with cme

 -- Dominique Dumont <dod@debian.org>  Sun, 27 Sep 2020 17:28:49 +0200

moarvm (2020.08+dfsg-1) unstable; urgency=low

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure URI in Homepage field.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Drop transition for old debug package migration.

  [ Dominique Dumont ]
  * New upstream version 2020.08+dfsg

 -- Dominique Dumont <dod@debian.org>  Thu, 03 Sep 2020 18:26:01 +0200

moarvm (2020.07+dfsg-1) experimental; urgency=medium

  * New upstream version 2020.07+dfsg

 -- Dominique Dumont <dod@debian.org>  Thu, 30 Jul 2020 15:10:07 +0200

moarvm (2020.06+dfsg-1) unstable; urgency=medium

  * README.source: mention that librhash cannot replace sha1 lib
  * New upstream version 2020.06+dfsg

 -- Dominique Dumont <dod@debian.org>  Sat, 27 Jun 2020 14:50:21 +0200

moarvm (2020.05+dfsg-1) unstable; urgency=medium

  * New upstream version 2020.05+dfsg
    (Closes: #957554)

 -- Dominique Dumont <dod@debian.org>  Sun, 10 May 2020 14:56:55 +0200

moarvm (2020.02.1+dfsg-1) unstable; urgency=medium

  * New upstream version 2020.02.1+dfsg
  * replace samcv key with Alexander Kiryuhin's
  * copyright: exclude 3rdparty/libatomicops
  * refreshed copyright
  * remove ftbs.patch (applied upstream)
  * control: declare compliance with policy 4.5.0

 -- Dominique Dumont <dod@debian.org>  Sun, 26 Apr 2020 17:26:28 +0200

moarvm (2019.11+dfsg-2) unstable; urgency=medium

  * Update my mail address.
  * Upload to unstable.

 -- Mo Zhou <lumin@debian.org>  Sat, 28 Dec 2019 15:15:51 +0800

moarvm (2019.11+dfsg-1) experimental; urgency=medium

  * New upstream version 2019.11+dfsg
  * Add patch to fix FTBFS with libtommath-dev (= 1.2.0).
  * override dh_missing to list missing files.

 -- Mo Zhou <cdluminate@gmail.com>  Thu, 26 Dec 2019 13:45:42 +0800

moarvm (2019.07.1+dfsg-2) unstable; urgency=medium

  * Apply wrap-and-sort.
  * Bump Standards-Version to 4.4.0 (no change).
  * B-D on debhelper-compat (= 12) and deprecate d/compat.
  * Upload to unstable.

 -- Mo Zhou <cdluminate@gmail.com>  Thu, 19 Sep 2019 01:49:08 +0000

moarvm (2019.07.1+dfsg-1) experimental; urgency=medium

  * New upstream version 2019.07.1+dfsg
  * Refresh patches.

 -- Mo Zhou <cdluminate@gmail.com>  Thu, 22 Aug 2019 06:24:34 +0000

moarvm (2019.03+dfsg-1) experimental; urgency=medium

  * New upstream version 2019.03+dfsg
  * Add myself to uploaders.
  * Refresh patches, and drop patches merged in the new release:
    - fix-for-new-ltm.patch: merged in new release.
    - fix-name-collision.patch: merged in new release.
    - fix_rakudo-build-failure-on-big-endian.diff: merged.
  * Install the new pkg-config file.

 -- Mo Zhou <cdluminate@gmail.com>  Fri, 12 Jul 2019 11:43:49 +0000

moarvm (2018.12+dfsg-3) unstable; urgency=medium

  * add upstream patch to fix symbol collision
  * add patch to fix compat with new libtommath (Closes: #921769)
  * fix.scanned.copyright: don't tweak removed file
  * fill.copyright.blanks: don't fill for removed file
  * fill.copyright.blanks: rm moar.pod cleanup.
  * update debian/copyright (cme)
  * bump compat to 12
  * declare compliance with policy 4.1.4

 -- Dominique Dumont <dod@debian.org>  Sat, 09 Feb 2019 16:56:12 +0100

moarvm (2018.12+dfsg-2) unstable; urgency=medium

  * Backport a commit that fixes "Cannot declare pseudo-package GLOBAL"
  * Fix typo in debian/README.source

 -- Robert Lemmen <robertle@semistable.com>  Sat, 29 Dec 2018 22:34:31 +0100

moarvm (2018.12+dfsg-1) unstable; urgency=medium

  * New upstream version 2018.12+dfsg

 -- Robert Lemmen <robertle@semistable.com>  Sat, 22 Dec 2018 14:25:48 +0100

moarvm (2018.11+dfsg-1) unstable; urgency=medium

  * New upstream version 2018.11+dfsg

 -- Robert Lemmen <robertle@semistable.com>  Fri, 30 Nov 2018 20:04:30 +0100

moarvm (2018.10+dfsg-1) unstable; urgency=medium

  * New upstream version 2018.10+dfsg

 -- Robert Lemmen <robertle@semistable.com>  Thu, 15 Nov 2018 13:48:40 +0100

moarvm (2018.09+dfsg-1) unstable; urgency=medium

  * New upstream version 2018.09+dfsg

 -- Robert Lemmen <robertle@semistable.com>  Mon, 24 Sep 2018 08:41:22 +0200

moarvm (2018.06+dfsg-1) unstable; urgency=medium

  * New upstream version 2018.06+dfsg

 -- Robert Lemmen <robertle@semistable.com>  Tue, 26 Jun 2018 20:36:10 +0200

moarvm (2018.05+dfsg-1) unstable; urgency=medium

  * New upstream version 2018.05+dfsg

 -- Robert Lemmen <robertle@semistable.com>  Sat, 26 May 2018 13:15:27 +0200

moarvm (2018.04.1+dfsg-1) unstable; urgency=medium

  * add patch to fix validator problems on big endian (R#1711)
  * New upstream version 2018.04.1+dfsg

 -- Robert Lemmen <robertle@semistable.com>  Mon, 30 Apr 2018 08:27:59 +0200

moarvm (2018.04+dfsg-1) unstable; urgency=medium

  * New upstream version 2018.04+dfsg
  * Remove patches now integrated upstream
  * Bump Standards-Version, add myself to uploaders

 -- Robert Lemmen <robertle@semistable.com>  Wed, 25 Apr 2018 17:28:45 +0200

moarvm (2018.03+dfsg-1) unstable; urgency=medium

  [ Robert Lemmen ]
  * patch nursery alignment for armhf
  * match libffi usage for s390x/ppc64

  [ Dominique Dumont ]
  * New upstream version 2018.03+dfsg
  * watch: search for .asc signature
  * control: update Vcs-Browser and Vcs-Git
  * update copyright (add 3rdparty/cmp)
  * update README.source (add 3rdparty/cmp)

 -- Dominique Dumont <dod@debian.org>  Tue, 03 Apr 2018 18:00:44 +0200

moarvm (2018.02+dfsg-2) unstable; urgency=medium

  * patch to fix big endian build for nqp
  * rules: harden build

 -- Dominique Dumont <dod@debian.org>  Thu, 15 Mar 2018 13:44:49 +0100

moarvm (2018.02+dfsg-1) unstable; urgency=medium

  * New upstream version 2018.02+dfsg
  * watch:
    * check tarball signature
    * use https url
    * bump to version 4

 -- Dominique Dumont <dod@debian.org>  Fri, 23 Feb 2018 17:11:18 +0100

moarvm (2018.01+dfsg-1) unstable; urgency=medium

  * New upstream version 2018.01+dfsg
  * fill.copyright.blanks: override 3rdparty/uthash.h license
  * refreshed copyright info with cme
  * control:
    * fix moarvm-dev dep list (Closes: #883599)
    * declare compliance with policy 4.1.3
    * depends on libuv1.* >= 1.18.0
  * rules:
    * fix PHONY build
    * pass -rpath with LDFLAGS
  * remove obsolete patches

 -- Dominique Dumont <dod@debian.org>  Tue, 06 Feb 2018 18:33:13 +0100

moarvm (2017.10+dfsg-1) unstable; urgency=medium

  * New upstream version 2017.10+dfsg
  * fix syntax issue in fill.copyright.blanks.yml
  * control: declare compliance with policy 4.1.1

 -- Dominique Dumont <dod@debian.org>  Wed, 01 Nov 2017 18:26:15 +0100

moarvm (2017.06+dfsg-1) unstable; urgency=medium

  * New upstream version 2017.06+dfsg
  * Patch: refresh
  * control: bump Standards-Version to 4.0.0

 -- Daniel Dehennin <daniel.dehennin@baby-gnu.org>  Sat, 24 Jun 2017 00:25:02 +0200

moarvm (2017.05+dfsg-1) experimental; urgency=medium

  [ Daniel Dehennin ]
  * New upstream version 2017.05+dfsg
  * Patch: refresh
  * Add patch to restore RPATH to the link flags
  * copyright: update with cme update

 -- Dominique Dumont <dod@debian.org>  Tue, 23 May 2017 17:04:56 +0200

moarvm (2017.03+dfsg-1) experimental; urgency=medium

  * New upstream version 2017.03+dfsg

 -- Dominique Dumont <dod@debian.org>  Fri, 31 Mar 2017 13:51:56 +0200

moarvm (2017.02+dfsg-1) experimental; urgency=medium

  * New upstream version 2017.02+dfsg
  * copyright: updated with cme dpkg update
  * removed obsolete fix-lib-install-dir patch
    and lintian overrides
  * rules: harden moar build

 -- Dominique Dumont <dod@debian.org>  Sun, 05 Mar 2017 16:03:03 +0100

moarvm (2016.12+dfsg-1) unstable; urgency=medium

  * New upstream version 2016.12+dfsg
  * refreshed patch

 -- Dominique Dumont <dod@debian.org>  Mon, 19 Dec 2016 13:43:16 +0100

moarvm (2016.11+dfsg-1) unstable; urgency=medium

  * New upstream version 2016.11+dfsg
  * removed patches that are applied upstream
  * refreshed patch

 -- Dominique Dumont <dod@debian.org>  Mon, 28 Nov 2016 13:18:29 +0100

moarvm (2016.10+dfsg-1) unstable; urgency=medium

  Many thanks to Tobias Leich (FROGGS) who did a terrific job
  porting moar on all architectures supported by Debian.
  * New upstream version 2016.10+dfsg
  * remove patches applied upstream
  + new patch: use ffi_arg type to fix powerpc build
    (Closes: #841343)
  + add patch to fix nqp tests on mips
  + added patch to fix build without dyncall source
  + added patch to fix s390x build
  * control:
    * enable s390x
    * enable mips mipsel mips64el (cf: #841346)
  * copyright: remove excluded dyncall (cme)

 -- Dominique Dumont <dod@debian.org>  Sat, 29 Oct 2016 18:25:01 +0200

moarvm (2016.09+dfsg-2) unstable; urgency=medium

  [ Daniel Dehennin ]
  * Use libffi for all architectures
  * New patch to fix libffi build
  * Import patch to fix build and tests on x32

  [ Dominique Dumont ]
  * added patch to fix floating point exception on armhf
  * imported patch to fix build tests on armhf

 -- Dominique Dumont <dod@debian.org>  Mon, 10 Oct 2016 09:06:44 +0200

moarvm (2016.09+dfsg-1) unstable; urgency=medium

  [ Daniel Dehennin ]
  * New upstream version 2016.09+dfsg
  * Set moarvm-dbg as Multi-Arch: same
  * Fix dyncall headers installation when using libffi
  * Generate automatic dbgsym package

  [ Dominique Dumont ]
  * cme files:
    * set up auto clean up of extracted © statement
    * work-around licensecheck bug
  * copyright: refreshed with cme
  * control: removed moarvm-dbg package
  * rules: only "build" is needed in .PHONY

 -- Dominique Dumont <dod@debian.org>  Tue, 27 Sep 2016 09:09:23 +0200

moarvm (2016.07+dfsg-1) unstable; urgency=medium

  * Mangle debian version in debian/watch file
  * Update source management documentation
  * Delete copyright tweaks for removed 3party sources
  * Use HTTPs for debian/copyright Format URL
  * Add “+dfsg” to Debian package upstream version
  * Imported Upstream version 2016.07+dfsg
  * copyright: updated with cme
  * control: update Vcs-Browser URL to use HTTPs

 -- Daniel Dehennin <daniel.dehennin@baby-gnu.org>  Sat, 23 Jul 2016 14:11:32 +0200

moarvm (2016.06+dfsg-1) unstable; urgency=medium

  * Imported Upstream version 2016.06+dfsg
    * removed libtommath source from package
  * patch out load step of libtommath objects during build
  * document status of convenience copies
  * refreshed patches

 -- Dominique Dumont <dod@debian.org>  Tue, 05 Jul 2016 10:39:25 +0200

moarvm (2016.05-3) unstable; urgency=medium

  * rules: use system libuv to compile
  * control: added Build-Depends on libuv1-dev
  * copyright: excludes libuv libtommath libatomics_ops copies
    (will be applied to next upstream release)
  * removed patch applied to excluded libuv

 -- Dominique Dumont <dod@debian.org>  Thu, 09 Jun 2016 18:20:24 +0200

moarvm (2016.05-2) unstable; urgency=medium

  * added lintian overrides for /usr/share/perl6/site/lib

 -- Dominique Dumont <dod@debian.org>  Thu, 02 Jun 2016 10:21:17 +0200

moarvm (2016.05-1) unstable; urgency=medium

  * Imported Upstream version 2016.05
  * let root write the doc file (Closes: #822834)
  * updated patch to search libraries in /usr/lib/perl6
    and /usr/share/perl6 (path set by upstream).

 -- Dominique Dumont <dod@debian.org>  Tue, 31 May 2016 13:45:58 +0200

moarvm (2016.04-1) unstable; urgency=medium

  * Imported Upstream version 2016.04
  * control: Standards-Version: '3.9.7' -> '3.9.8'

 -- Dominique Dumont <dod@debian.org>  Sat, 23 Apr 2016 15:36:04 +0200

moarvm (2016.03-1) unstable; urgency=medium

  * Imported Upstream version 2016.03
  * copyright: updated with cme
  * removed add-min-max-macro patch (applied upstream)

 -- Dominique Dumont <dod@debian.org>  Sat, 02 Apr 2016 11:51:34 +0200

moarvm (2016.02-1) unstable; urgency=medium

  * Imported Upstream version 2016.02
  * refreshed patch to port libuv to kfreebsd arch
  * control: updated Vcs-Git to use https (cme)
  * control: updated Standards-Version to 3.9.7 (cme)
  + added copyright tweaks files for cme update dpkg-copyright
  * updated debian/copyright with cme.

 -- Dominique Dumont <dod@debian.org>  Wed, 24 Feb 2016 11:55:56 +0100

moarvm (2015.12-1) unstable; urgency=medium

  * Imported Upstream version 2015.12
  * refreshed patch

 -- Dominique Dumont <dod@debian.org>  Wed, 13 Jan 2016 12:10:06 +0100

moarvm (2015.11-2) unstable; urgency=medium

  * control: removed mipsel from list of arch (Closes: #809207)

 -- Dominique Dumont <dod@debian.org>  Mon, 04 Jan 2016 20:56:08 +0100

moarvm (2015.11-1) unstable; urgency=medium

  [ Daniel Dehennin ]
  * Imported Upstream version 2015.10
  * Imported Upstream version 2015.11

  [ Dominique Dumont ]
  * Added patch to fix armhf build

 -- Daniel Dehennin <daniel.dehennin@baby-gnu.org>  Sun, 29 Nov 2015 22:14:56 +0100

moarvm (2015.09-3) unstable; urgency=medium

  * added patch to search ELF lib in /usr/lib/perl6 instead
    of /usr/share/perl6. rakudo must have a similar patch.
  * updated lintian-override for new lib install path

 -- Dominique Dumont <dod@debian.org>  Wed, 07 Oct 2015 19:34:46 +0200

moarvm (2015.09-2) unstable; urgency=medium

  * control: Created moarvm-dev package to deliver headers files
    required to build rakudo

 -- Dominique Dumont <dod@debian.org>  Sat, 03 Oct 2015 21:00:15 +0200

moarvm (2015.09-1) unstable; urgency=medium

  * Imported Upstream version 2015.09

 -- Dominique Dumont <dod@debian.org>  Sat, 03 Oct 2015 12:52:21 +0200

moarvm (2015.07-1) unstable; urgency=medium

  * Imported Upstream version 2015.07
  * Enable mipsel architecture (Closes: #789187)
    Thanks to Arturo Borrero Gonzalez

 -- Daniel Dehennin <daniel.dehennin@baby-gnu.org>  Sun, 26 Jul 2015 15:19:35 +0200

moarvm (2015.06-2) unstable; urgency=medium

  * control: add libkvm-dev Build-Depends for kfreebsd.
    Thanks to Tobias Leich

 -- Daniel Dehennin <daniel.dehennin@baby-gnu.org>  Tue, 23 Jun 2015 18:43:30 +0200

moarvm (2015.06-1) unstable; urgency=medium

  [ Daniel Dehennin ]
  * Imported upstream version 2015.06
  * control: limit supported architectures to Intel, ARM and ppc
    (Closes: #789187)
  * Patches:
    * Updated for 2015.06
    + Added patch to support kfreebsd
  * README.source:
    * Explain source management with git-buildpackage
    * Document debian/changelog management with gbp-dch

  [ Paul Cochrane ]
  * Override the lintian RPATH check
  * README.source: minor textual corrections

  [ Dominique Dumont ]
  * copyright (Tx Ansgar):
    * corrected BSD variant of 2 files from 3rdparty directory
    * added © entry for file generated from Unicode Inc database
    * use a more accurate license keyword for BSD like license

 -- Daniel Dehennin <daniel.dehennin@baby-gnu.org>  Mon, 22 Jun 2015 00:00:17 +0200

moarvm (2015.04-1) unstable; urgency=medium

  [ Paul Cochrane ]
  * Add initial notes about building moarvm package
  * Remove trailing whitespace
  * Patch Configure.pl to determine version string correctly

  [ Daniel Dehennin ]
  * Add description to quilt patch
  * Imported Upstream version 2015.04
  * Set maintainer to Debian Rakudo Maintainers

 -- Daniel Dehennin <daniel.dehennin@baby-gnu.org>  Sun, 26 Apr 2015 23:17:53 +0200

moarvm (2015.03-1) unstable; urgency=medium

  * Initial release (Closes: #750837)

 -- Daniel Dehennin <daniel.dehennin@baby-gnu.org>  Sat, 14 Mar 2015 21:50:28 +0100
